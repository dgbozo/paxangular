import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BagagesComponent } from './bagages.component';

describe('BagagesComponent', () => {
  let component: BagagesComponent;
  let fixture: ComponentFixture<BagagesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BagagesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BagagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
