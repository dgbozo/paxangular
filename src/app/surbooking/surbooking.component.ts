import { Component, ViewContainerRef } from '@angular/core';
import { FormBuilder, FormGroup, UntypedFormGroup, Validators } from '@angular/forms';
import { AllServiceService } from '../service/all-service.service';
import { Listepays } from '../model/listepays';
import { Airports } from '../model/airports';
import { Surbooking } from '../model/surbooking';
import {  NzMessageService } from 'ng-zorro-antd/message';
import { Router } from '@angular/router';
import { concatMap, map } from 'rxjs/operators';
@Component({
  selector: 'app-surbooking',
  templateUrl: './surbooking.component.html',
  styleUrls: ['./surbooking.component.css']
})
export class SurbookingComponent {
  imageDisplay!: string | ArrayBuffer | null;
  acteurForm!: FormGroup;
  activiteEconomiqueForm!: FormGroup;
  activiteEconomiqueDocumentForm!: FormGroup;
  isActionInProgress: boolean = false;
  fileUploaded=false;
  fileUploading=false;
  fileUploaded1=false;
  fileUploading1=false;
  currentPage: number = 0;
  isLoading = false;
  telephone!: string;
     // stepper variables
     isSpinning =false;

   selectedTabIndex: number = 0;
   current_step_index = 0;
   last_step_index = 2;
   step_index_description = 'First-content';
   filesL: any;


   files1: any;

   submitted = false;

   data: any;


   value?: string;

   validateForm!: UntypedFormGroup;

   radioValue = 'A';
   radioValue1 = 'A';
   radioValue2 = 'A';
   selectedValue = null;
   selectedValue1 = null;
   selectedValue2 = null;
   listepays: Array<Listepays>=[];
  airports: Array<Airports>=[];
  constructor(private fb: FormBuilder, public allService:AllServiceService, private viewContainerRef: ViewContainerRef,private message: NzMessageService,private route: Router) {

  }

  ngOnInit(): void {
    this.AllPays();
    this.AllAirports();
    this.validateForm = this.fb.group({
      formLayout: ['vertical'],

      userTel: [null, [Validators.required]],
      dbvoyage: [null,[Validators.required]],
      fnvoyage: [null, [Validators.required]],
      nreservation: [null,[Validators.required]],
      compagnie: [null,[ Validators.required]],
      lieupb: ['', [Validators.required]],
      probleme: ['Surbooking',[ Validators.required]],
      vol: ['',[ Validators.required]],
      date_du_vol: ['', [Validators.required ]],
      formation: this.fb.group({

       volontaire: ['non', [Validators.required]],
       appelvolontaire: ['non', [Validators.required]],
       refus_embarquement: ["non", [Validators.required]],
       retard: ['', [Validators.required]],
     remboursement: ["non", [Validators.required]],
       montant: ['', [Validators.required]]

      }), dossier: this.fb.group({

        nom: ['', [Validators.required]],
        prenom: ['', [Validators.required]],
        adresse: ['', [Validators.required]],
        email: ['', [Validators.email, Validators.required]],
        ville: ['', [Validators.required]],
        pays: ['', [Validators.required]],
        contact: ['', [Validators.required]]

      }), assist: this.fb.group({

        assistance: ['', [Validators.required]],
        reservation: [false, [Validators.required]],
        droit_information: [false, [Validators.required]]


      }), fin: this.fb.group({

           /*
        cni: ['', [Validators.required]], cv: ['', [Validators.required]],
        motivation: ['', [Validators.required]]
        attestationDeclaration: [],
        autredoc: [],*/
        cni: ['', [Validators.required]],
        carteEmbarquement: ['', [Validators.required]],


      })
    });
  }



  AllPays(){
    this.allService.getAllpays().subscribe({
      next:data=>{
        this.listepays=data
      },error:err=>{
        console.log(err);
      }
    });
  }
  AllAirports(){
    this.allService.getAllairport().subscribe({
      next:data=>{
        this.airports=data
      },error:err=>{
        console.log(err);
      }
    });
  }

  uploadLettre(event: any) {
    this.fileUploading=true;
    this.filesL= event.target.files[0];
    if(this.filesL!=null){
      this.validateForm.get('fin.cni')?.setValue(this.filesL.name);

    }

    this.fileUploading=false;
    this.fileUploaded=true;
    console.log(this.filesL);

  }
  uploadEmbarquement(event :any) {
    this.fileUploading1=true;
    this.files1= event.target.files[0];
    if(this.files1!=null){
      this.validateForm.get('fin.carteEmbarquement')?.setValue(this.files1.name);

    }
    this.fileUploading1=false;
    this.fileUploaded1=true;
    console.log(this.files1);

  }

  private _addSurbooking(annulation: Surbooking) {
    this.allService.createSurbooking(annulation).subscribe(


    );
  }

  previous(): void {
    this.current_step_index -= 1;
  }

// FIN GESTION DES ACTIVITES ECONOMIQUES

    // Gestion du stepper
    pre(): void {
      this.current_step_index -= 1;

  }

  next(): void {
      this.current_step_index += 1;

  }

  done(): void {
    this.current_step_index += 1;

  }
  submitForm(): void {


    if ( this.current_step_index!= 5) return;
    this.isSpinning = true;
   console.log('submit', this.validateForm.value);


    const formData = new FormData();
    formData.append("dbvoyage",this.validateForm.value['dbvoyage']);
    formData.append("fnvoyage", this.validateForm.value['fnvoyage']);
    formData.append("lieupb", this.validateForm.value['lieupb']);
    formData.append("compagnie", this.validateForm.value['compagnie']);
    formData.append("date_du_vol", this.validateForm.value['date_du_vol']);
    formData.append("vol", this.validateForm.value['vol']);
    formData.append("nreservation", this.validateForm.value['nreservation']);
    formData.append("probleme", this.validateForm.value['probleme']);




    formData.append("volontaire", this.validateForm.get('formation')?.value['volontaire']);
    formData.append("appelvolontaire", this.validateForm.get('formation')?.value['appelvolontaire']);
    formData.append("refus_embarquement", this.validateForm.get('formation')?.value['refus_embarquement']);
    formData.append("retard", this.validateForm.get('formation')?.value['retard']);
    formData.append("remboursement", this.validateForm.get('formation')?.value['remboursement']);



    formData.append("montant", this.validateForm.get('formation')?.value['montant']);
    formData.append("nom", this.validateForm.get('dossier')?.value['nom']);
    formData.append("prenom", this.validateForm.get('dossier')?.value['prenom']);

    formData.append("tel", this.validateForm.get('dossier')?.value['contact']);
    formData.append("adresse", this.validateForm.get('dossier')?.value['adresse']);
    formData.append("pays", this.validateForm.get('dossier')?.value['pays']);
    formData.append("ville", this.validateForm.get('dossier')?.value['ville']);
    formData.append("email", this.validateForm.get('dossier')?.value['email']);


    formData.append("assistance", this.validateForm.get('assist')?.value['assistance']);
    formData.append("reservation", this.validateForm.get('assist')?.value['reservation']);
    formData.append("droit_information", this.validateForm.get('assist')?.value['droit_information']);



    /*formData.append("justifs", this.validateForm.value[' justifs']);
    formData.append("banque", this.validateForm.value['banque']);
    formData.append("carteEmbarquement", this.validateForm.value['carteEmbarquement']);
    formData.append("autredoc", this.validateForm.value['autredoc']);
    formData.append("attestationDeclaration", this.validateForm.value['attestationDeclaration']);*/
    formData.append("cni", this.filesL, this.filesL.name);
    formData.append("carteEmbarquement", this.files1, this.files1.name);


    this.allService.createSurbooking(formData).subscribe(res => {
      this.data = res;

      if(this.data.status=true){
        this.message
        .loading('Action in progress', { nzDuration: 2500 })
        .onClose!.pipe(
          concatMap(() => this.message.success('Encours d\'envoie', { nzDuration: 2500 }).onClose!),
          concatMap(() => this.message.info('Demande  envoyer,Veuillez consulter votre boite email ', { nzDuration: 2500 }).onClose!)
        )
        .subscribe(() => {
          console.log('All completed!');

        });
        this.validateForm.reset();
        this.isSpinning = false;
        this.route.navigate(['/']);
      }else{
        this.message
        .loading('Action in progress', { nzDuration: 2500 })
        .onClose!.pipe(
          concatMap(() => this.message.success('Encours d\'envoie', { nzDuration: 2500 }).onClose!),
          concatMap(() => this.message.error('Une erreur s\'est produite', { nzDuration: 2500 }).onClose!)
        )
        .subscribe(() => {
          console.log('Envoi echoue!');
        });
      }
      this.isSpinning = false;
      this.route.navigate(['/']);
      this.validateForm.reset();
      console.log(this.data);
    })
}
}

