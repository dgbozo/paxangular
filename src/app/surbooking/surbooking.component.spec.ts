import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SurbookingComponent } from './surbooking.component';

describe('SurbookingComponent', () => {
  let component: SurbookingComponent;
  let fixture: ComponentFixture<SurbookingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SurbookingComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SurbookingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
