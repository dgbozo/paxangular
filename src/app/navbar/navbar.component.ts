import { Component } from '@angular/core';
import { AuthentificationServiceService } from '../service/authentification-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {
  constructor(public authService:AuthentificationServiceService,private router:Router ){}
  ngOnInit(): void {}
cacheOuAfficheMenu(){
  if (localStorage.getItem("authUser")) {
    return false;
  }
  return true;
}
}
