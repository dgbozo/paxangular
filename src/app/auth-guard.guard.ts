import { Injectable, inject } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivateFn, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { AuthentificationServiceService } from './service/authentification-service.service';
import { Observable } from 'rxjs';

/*

@Injectable()
 export class PermissionsService {

 constructor(
  private authservice: AuthentificationServiceService,private router:Router
 ) { }

 canActivate(
  route: ActivatedRouteSnapshot,
  state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    let authenticate=this.authservice.isAuthenticated();
    if(authenticate==false){
      this.router.navigateByUrl("/login");
      return false;
    }else{

      return true;
    }

}


 }*/

export const authGuardGuard: CanActivateFn = (route, state) => {
const router=inject(Router);
  let isloged=localStorage.getItem("authUser");
  if (isloged) {
    return true;

  }   router.navigateByUrl("/login");
  return false;
};
