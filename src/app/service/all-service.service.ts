import { Injectable } from '@angular/core';
import { Annulation } from '../model/annulation';
import { HttpClient ,HttpHeaders} from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { ValidationErrors } from '@angular/forms';
import { Listepays } from '../model/listepays';
import { Airports } from '../model/airports';
import { Bagage } from '../model/bagage';
import { Retard } from '../model/retard';
import { Surbooking } from '../model/surbooking';

@Injectable({
  providedIn: 'root'
})
export class AllServiceService {
  private annulation! : Array<Annulation> ;
  private listepays!: Array<Listepays>;
  private Airports ! : Array<Airports> ;
  constructor(private http: HttpClient) { }

  public addNewAnnulation(product :Annulation): Observable<Annulation>{
    // product.id=UUID.UUID();
    return this.http.post<Annulation>("http://localhost:3000/products",product);

     }
     getErrorMessage(fieldName: string , error: ValidationErrors) : string {
      if (error['required']) {
        return fieldName + "is required";
      }else if (error['minLenght']) {
        return fieldName + " should have at least"+ error['minlenght']['requiredLenght']+" characters";
      } else if (error['min']) {
        return fieldName + " should have min value"+ error['min']['min']+" characters";
      }else return"";
      }

      /* traitement annulation pour admin*/
     public getListeAnnulation():Observable<Array<Annulation>>{
        return   this.http.get<Array<Annulation>>("https://bfcao.com.sapiens.ml/pax/test/public/api/annulations");
      }


        public getAnnulation(id:number):Observable<any>{
        /*  console.log(this.annulation);
          console.log("grty");
        let annulation= this.annulation.find(p=>p.id==id);
        if (annulation==undefined) return throwError(()=>new Error ("product not find"))


        return of(annulation); */
        return this.http.get("https://bfcao.com.sapiens.ml/pax/test/public/api/annulations/"+id);
       }
/* traitement bagage pour admin*/
       public getListeBagage():Observable<Array<Bagage>>{
        return   this.http.get<Array<Bagage>>("https://bfcao.com.sapiens.ml/pax/test/public/api/bagages");
      }

      public getBagage(id:number):Observable<any>{
        /*  console.log(this.annulation);
          console.log("grty");
        let annulation= this.annulation.find(p=>p.id==id);
        if (annulation==undefined) return throwError(()=>new Error ("product not find"))


        return of(annulation); */
        return this.http.get("https://bfcao.com.sapiens.ml/pax/test/public/api/bagages/"+id);
       }
/**fin traitement bagages */

/* traitement retard pour admin*/
public getListeRetard():Observable<Array<Retard>>{
  return   this.http.get<Array<Retard>>("https://bfcao.com.sapiens.ml/pax/test/public/api/retard");
}

public getRetard(id:number):Observable<any>{
  /*  console.log(this.annulation);
    console.log("grty");
  let annulation= this.annulation.find(p=>p.id==id);
  if (annulation==undefined) return throwError(()=>new Error ("product not find"))


  return of(annulation); */
  return this.http.get("https://bfcao.com.sapiens.ml/pax/test/public/api/retard/"+id);
 }
/**fin traitement retard*/

/* traitement surbooking pour admin*/
public getListeSurbooking():Observable<Array<Surbooking>>{
  return   this.http.get<Array<Surbooking>>("https://bfcao.com.sapiens.ml/pax/test/public/api/surbooking");
}

public getSurbooking(id:number):Observable<any>{
  /*  console.log(this.annulation);
    console.log("grty");
  let annulation= this.annulation.find(p=>p.id==id);
  if (annulation==undefined) return throwError(()=>new Error ("product not find"))


  return of(annulation); */
  return this.http.get("https://bfcao.com.sapiens.ml/pax/test/public/api/surbooking/"+id);
 }
/**fin traitement surbooking*/

      public getAllpays():Observable<Array<Listepays>>{
        return   this.http.get<Array<Listepays>>("https://bfcao.com.sapiens.ml/pax/test/public/api/listepays");
      }

      public setAllbagage(f:Annulation):Observable<Array<Airports>>{
        return   this.http.get<Array<Airports>>("https://bfcao.com.sapiens.ml/pax/test/public/api/airports");
      }
      public getAllairport():Observable<Array<Airports>>{
        return   this.http.get<Array<Airports>>("https://bfcao.com.sapiens.ml/pax/test/public/api/airports");
      }

      createAnnulation(f:any): Observable<Annulation> {
        return this.http.post<Annulation>("https://bfcao.com.sapiens.ml/pax/test/public/api/annulations", f);
      }
      createBagage(f:any): Observable<Bagage> {
        return this.http.post<Bagage>("https://bfcao.com.sapiens.ml/pax/test/public/api/bagages", f);
      }
      createRetard(f:any): Observable<Retard> {
        return this.http.post<Retard>("https://bfcao.com.sapiens.ml/pax/test/public/api/retard", f);
      }

      createSurbooking(f:any): Observable<Surbooking> {
        return this.http.post<Surbooking>("https://bfcao.com.sapiens.ml/pax/test/public/api/surbooking", f);
      }

      calculRetard(f:any){
        return this.http.post("https://bfcao.com.sapiens.ml/pax/test/public/api/calculretard",f);
      }
      calculAnnulation(f:any){
        return this.http.post("https://bfcao.com.sapiens.ml/pax/test/public/api/calculannulation",f);
      }
      calculSurbooking(f:any){
        return this.http.post("https://bfcao.com.sapiens.ml/pax/test/public/api/calculsurbooking",f);
      }
      calculBagage(f:any){
        return this.http.post("https://bfcao.com.sapiens.ml/pax/test/public/api/calculbagage",f);
      }
      getStatutDemande(f:any){
        return this.http.post("https://bfcao.com.sapiens.ml/pax/test/public/api/getstatutdemande",f);
      }
      sendContact(f:any){
        return this.http.post("https://bfcao.com.sapiens.ml/pax/test/public/api/contact",f);
      }

}
