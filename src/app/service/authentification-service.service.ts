import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { User } from '../model/user';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthentificationServiceService {
  url: string = environment.backend;
 
  authentificadUser : User | undefined;

  constructor(    private http: HttpClient) {}

  login(f: any): Observable<User> {
    return this.http.post<User>("https://bfcao.com.sapiens.ml/pax/test/public/api/login",f);
  }
  public authenticateUser(appUser: User):Observable<boolean>{
    this.authentificadUser=appUser;
    localStorage.setItem("authUser",JSON.stringify({name:appUser.name,email:appUser.email,jwt:"JWT_TOKEN"}));
    return of(true);

   }
   public isAuthenticated(){
    return this.authentificadUser!=undefined;
   }
   public logout():Observable<boolean>{
    this.authentificadUser=undefined;
    localStorage.removeItem("authUser");
    return of(true);
   }

}
