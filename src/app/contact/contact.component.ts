import { Component, ViewContainerRef } from '@angular/core';
import { FormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { AllServiceService } from '../service/all-service.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { Router } from '@angular/router';
import { NzModalService } from 'ng-zorro-antd/modal';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent {
  isSpinning = false;
  validateForm!: UntypedFormGroup;
  data: any;
  constructor(private fb: FormBuilder, public allService:AllServiceService, private viewContainerRef: ViewContainerRef,private message: NzMessageService,private route: Router,private modal: NzModalService) {

  }
  ngOnInit(): void {




    this.validateForm = this.fb.group({
      formLayout: ['vertical'],


      name: ['',[Validators.required]],
      email: ['', [Validators.required]],
     subject: ['', [Validators.required]],
     message: ['', [Validators.required]],



  });
}
success(h:any): void {
  this.modal.success({
    nzTitle: 'Félicitation',
    nzContent: h
  });
}
error(): void {
  this.modal.error({
    nzTitle: '  Oups  ',
    nzContent: 'désolé votre message n\'a pas été pris en compte. Veuillez réessayer'
  });
}
submitForm(): void {
  console.log('submit', this.validateForm.value);
  this.isSpinning = true;

const formData = new FormData();
formData.append("name",this.validateForm.value['name']);
formData.append("email", this.validateForm.value['email']);
formData.append("subject", this.validateForm.value['subject']);
formData.append("message", this.validateForm.value['message']);
/*this.allService.sendContact(formData).subscribe(res => {
  this.data = res;

  if (this.data.success) {
    this.isSpinning = false;
    this.success(this.data.success);
  }else{
    this.isSpinning = false;
    this.error();
  }

});*/
this.allService.sendContact(formData).subscribe({
  next:(res)=>{
    this.data = res;
    this.isSpinning = false;
    this.success(this.data.success);
  },
  error:(err)=>{
    this.isSpinning = false;
    this.error();
  }
})

this.validateForm.reset();
}
}
