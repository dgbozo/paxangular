import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthentificationServiceService } from '../service/authentification-service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  userFormGroup!: FormGroup;
  errorMessage!: any;
  constructor( private fb:FormBuilder,private authService:  AuthentificationServiceService,private router: Router){

  }
  ngOnInit(): void{
this.userFormGroup=this.fb.group({
  email: this.fb.control(""),
  password: this.fb.control("")
})


}

HandleLogin() {

  const formData = this.userFormGroup.value;


  this.authService.login(formData).subscribe({
   next:(appUser)=>{
    console.log(appUser);
           this.authService.authenticateUser(appUser).subscribe({
             next:(data)=>{
               this.router.navigateByUrl("/admin/listeAnnulation");
             }
           });
   },error:(err)=>{
     this.errorMessage=err;
   }
  })
     }

}
