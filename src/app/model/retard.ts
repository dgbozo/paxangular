export interface Retard {
id:string,
  nom:string,
  prenom:string,
  ville:string,
  email:string,
  adresse:string,
  tel:string,
  pays:string,
  dbvoyage:string,
  fnvoyage:string,
  nreservation:string,
  compagnie:string,
  lieupb:string,
  probleme:string,
  vol:string,
delai:string,
  date_du_vol: Date,
  assistance:string,
  cni: string,
  carteEmbarquement:string,

  droit_information:string,
  reservation:string,

  choix_remboursement:string,
  maintenu:string,
  compensationfinance :string,
  raison:string,
  retard:string,


  justifs :string,

  attestationDeclaration :string,
  banque :string,
  autredoc :string,

  created_at:string,
  numeroDemande:string,
  status:string,
}
