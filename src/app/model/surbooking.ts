export  interface Surbooking {
 id:any,
  nom:string,
  prenom:string,
  ville:string,
  email:string,
  adresse:string,
  tel:string,
  pays:string,
  dbvoyage:string,
  fnvoyage:string,
  nreservation:string,
  compagnie:string,
  lieupb:string,
  probleme:string,
  vol:string,
  passager_retard:string,
  embarquement_refuse_compagnie:string,
  compensationfinance:string,
  choix_remboursement:string,
  date_du_vol: Date,
  assistance:string,
  cni: string,
  volontaire:string,
  appel_volontaire:string,
  reservation_confirmez:string,
  created_at:string,
  numeroDemande:string,
  status:string,
}
