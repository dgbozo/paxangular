export interface Bagage {
  id:any,
  nom:string,
  prenom:string,
  ville:string,
  email:string,
  adresse:string,
  tel:string,
  pays:string,

  dbvoyage:string,
  fnvoyage:string,
  nreservation:string,
  compagnie:string,
  lieupb:string,
  probleme:string,
  vol:string,
  date_du_vol: Date,

  numero_billet:string,
  poids:number,
  bagage_total:number,
  bagage_revendique:number,
  montant:string,
bagage:string,
  assistance:string,
  droit_information:string,
  reservation:string,




  justifs :string,
  attestationDeclaration :string,
  banque :string,
  autredoc :string,
  cni: string,

  carteEmbarquement:string,

  created_at:string,
  numeroDemande:string,
  status:string,

}
