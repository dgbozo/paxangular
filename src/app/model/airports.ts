export interface Airports {
  id:number,
  nom:string,
  ville:string,
  pays:string,
  iata:string,
  icao:string,
  latitude:string,
  logetude:string,
  altitude:string,
  timezone:string

}
