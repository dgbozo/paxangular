import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailRetardComponent } from './detail-retard.component';

describe('DetailRetardComponent', () => {
  let component: DetailRetardComponent;
  let fixture: ComponentFixture<DetailRetardComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DetailRetardComponent]
    });
    fixture = TestBed.createComponent(DetailRetardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
