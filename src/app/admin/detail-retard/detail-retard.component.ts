import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Retard } from 'src/app/model/retard';
import { AllServiceService } from 'src/app/service/all-service.service';

@Component({
  selector: 'app-detail-retard',
  templateUrl: './detail-retard.component.html',
  styleUrls: ['./detail-retard.component.css']
})
export class DetailRetardComponent {
  retardId!:number;
  retard!:Retard;
  isSpinning =false;
  constructor( public allService:AllServiceService  ,private route:ActivatedRoute) {
    this.retardId=this.route.snapshot.params['id'];
  }


  ngOnInit():void{
    console.log(this.retardId);
    console.log(this.retard); this.isSpinning = true;
    this.allService.getRetard(this.retardId).subscribe({
      next:(retard)=>{
    this.retard=retard;
    this.isSpinning = false;
      },
      error:(err)=>{
        console.log(err); this.isSpinning = false;
      }
    })
}
}
