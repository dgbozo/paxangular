import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailAnnulationComponent } from './detail-annulation.component';

describe('DetailAnnulationComponent', () => {
  let component: DetailAnnulationComponent;
  let fixture: ComponentFixture<DetailAnnulationComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DetailAnnulationComponent]
    });
    fixture = TestBed.createComponent(DetailAnnulationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
