import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AllServiceService } from 'src/app/service/all-service.service';
import { Annulation } from 'src/app/model/annulation';
@Component({
  selector: 'app-detail-annulation',
  templateUrl: './detail-annulation.component.html',
  styleUrls: ['./detail-annulation.component.css']
})
export class DetailAnnulationComponent {
  annulationId!:number;
  annulation!:Annulation;
  isSpinning =false;
  constructor( public allService:AllServiceService  ,private route:ActivatedRoute) {
    this.annulationId=this.route.snapshot.params['id'];
  }
  ngOnInit():void{
    console.log(this.annulationId);
    console.log(this.annulation); this.isSpinning = true;
    this.allService.getAnnulation(this.annulationId).subscribe({
      next:(annulation)=>{
    this.annulation=annulation;
    this.isSpinning = false;
      },
      error:(err)=>{
        console.log(err); this.isSpinning = false;
      }
    })
    }
   
}
