import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Bagage } from 'src/app/model/bagage';
import { AllServiceService } from 'src/app/service/all-service.service';

@Component({
  selector: 'app-detail-bagage',
  templateUrl: './detail-bagage.component.html',
  styleUrls: ['./detail-bagage.component.css']
})
export class DetailBagageComponent {
  bagageId!:number;
  bagage!:Bagage;
  isSpinning =false;
  constructor( public allService:AllServiceService  ,private route:ActivatedRoute) {
    this.bagageId=this.route.snapshot.params['id'];
  }


  ngOnInit():void{
    console.log(this.bagageId);
    console.log(this.bagage); this.isSpinning = true;
    this.allService.getBagage(this.bagageId).subscribe({
      next:(bagage)=>{
    this.bagage=bagage;
    this.isSpinning = false;
      },
      error:(err)=>{
        console.log(err); this.isSpinning = false;
      }
    })
}
}