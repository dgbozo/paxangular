import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailBagageComponent } from './detail-bagage.component';

describe('DetailBagageComponent', () => {
  let component: DetailBagageComponent;
  let fixture: ComponentFixture<DetailBagageComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DetailBagageComponent]
    });
    fixture = TestBed.createComponent(DetailBagageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
