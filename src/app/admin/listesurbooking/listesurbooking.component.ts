import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { Surbooking } from 'src/app/model/surbooking';
import { AllServiceService } from 'src/app/service/all-service.service';

@Component({
  selector: 'app-listesurbooking',
  templateUrl: './listesurbooking.component.html',
  styleUrls: ['./listesurbooking.component.css']
})
export class ListesurbookingComponent {
  public surbookings!: Array<Surbooking> ;
  constructor(private fb: FormBuilder, public allService:AllServiceService  ,private router: Router) {

  } ngOnInit(): void {
    this.AllBagage();
  }
  AllBagage(){
    this.allService.getListeSurbooking().subscribe({
      next:data=>{
        this.surbookings=data;
      },error:err=>{
        console.log(err);
      }
    });
  }
  handleVoirplus(data: Surbooking) {
    this.router.navigateByUrl("admin/detailSurbooking/"+data.id);
    }
}
