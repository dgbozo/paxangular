import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListesurbookingComponent } from './listesurbooking.component';

describe('ListesurbookingComponent', () => {
  let component: ListesurbookingComponent;
  let fixture: ComponentFixture<ListesurbookingComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ListesurbookingComponent]
    });
    fixture = TestBed.createComponent(ListesurbookingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
