import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Surbooking } from 'src/app/model/surbooking';
import { AllServiceService } from 'src/app/service/all-service.service';

@Component({
  selector: 'app-detail-surbooking',
  templateUrl: './detail-surbooking.component.html',
  styleUrls: ['./detail-surbooking.component.css']
})
export class DetailSurbookingComponent {
  surbookingId!:number;
  surbooking!:Surbooking;
  isSpinning =false;

  constructor( public allService:AllServiceService  ,private route:ActivatedRoute) {
    this.surbookingId=this.route.snapshot.params['id'];
  }

  ngOnInit():void{
    console.log(this.surbookingId);
    console.log(this.surbooking); this.isSpinning = true;
    this.allService.getSurbooking(this.surbookingId).subscribe({
      next:(surbooking)=>{
    this.surbooking=surbooking;
    this.isSpinning = false;
      },
      error:(err)=>{
        console.log(err); this.isSpinning = false;
      }
    })
}
}
