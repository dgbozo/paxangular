import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailSurbookingComponent } from './detail-surbooking.component';

describe('DetailSurbookingComponent', () => {
  let component: DetailSurbookingComponent;
  let fixture: ComponentFixture<DetailSurbookingComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DetailSurbookingComponent]
    });
    fixture = TestBed.createComponent(DetailSurbookingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
