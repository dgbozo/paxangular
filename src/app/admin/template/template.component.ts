import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthentificationServiceService } from 'src/app/service/authentification-service.service';

@Component({
  selector: 'app-template',
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.css']
})
export class TemplateComponent {
  constructor(public authService:AuthentificationServiceService,private router:Router ){}
  ngOnInit(): void{
    
  }
  handlelogout() {
    this.authService.logout().subscribe({
      next:(data)=>{
  this.router.navigateByUrl("/login");
      }
    });
    }
}
