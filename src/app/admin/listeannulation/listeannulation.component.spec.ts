import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeannulationComponent } from './listeannulation.component';

describe('ListeannulationComponent', () => {
  let component: ListeannulationComponent;
  let fixture: ComponentFixture<ListeannulationComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ListeannulationComponent]
    });
    fixture = TestBed.createComponent(ListeannulationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
