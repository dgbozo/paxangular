import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { NzIconService } from 'ng-zorro-antd/icon';
import { Annulation } from 'src/app/model/annulation';
import { AllServiceService } from 'src/app/service/all-service.service';

@Component({
  selector: 'app-listeannulation',
  templateUrl: './listeannulation.component.html',
  styleUrls: ['./listeannulation.component.css']
})
export class ListeannulationComponent {

  public annulations!: Array<Annulation> ;
  constructor(private fb: FormBuilder, public allService:AllServiceService  ,private router: Router, private nzIconService: NzIconService) {

  }
  ngOnInit(): void {
    this.AllAnnulation();
  }
  AllAnnulation(){
    this.allService.getListeAnnulation().subscribe({
      next:data=>{
        this.annulations=data;
      },error:err=>{
        console.log(err);
      }
    });
  }
  handleEditProduct(data: Annulation) {
    this.router.navigateByUrl("admin/detailAnnulation/"+data.id);
    }
}
