import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { Retard } from 'src/app/model/retard';
import { AllServiceService } from 'src/app/service/all-service.service';

@Component({
  selector: 'app-listeretard',
  templateUrl: './listeretard.component.html',
  styleUrls: ['./listeretard.component.css']
})
export class ListeretardComponent {
  public retards!: Array<Retard> ;
  constructor(private fb: FormBuilder, public allService:AllServiceService  ,private router: Router) {

  }

  ngOnInit(): void {
    this.AllRetard();
  }
  AllRetard(){
    this.allService.getListeRetard().subscribe({
      next:data=>{
        this.retards=data;
      },error:err=>{
        console.log(err);
      }
    });
  }
  handleVoirplus(data: Retard) {
    this.router.navigateByUrl("admin/detailRetard/"+data.id);
    }
}
