import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeretardComponent } from './listeretard.component';

describe('ListeretardComponent', () => {
  let component: ListeretardComponent;
  let fixture: ComponentFixture<ListeretardComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ListeretardComponent]
    });
    fixture = TestBed.createComponent(ListeretardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
