import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { Bagage } from 'src/app/model/bagage';

import { AllServiceService } from 'src/app/service/all-service.service';

@Component({
  selector: 'app-listebagage',
  templateUrl: './listebagage.component.html',
  styleUrls: ['./listebagage.component.css']
})
export class ListebagageComponent {

  public bagages!: Array<Bagage> ;
  constructor(private fb: FormBuilder, public allService:AllServiceService  ,private router: Router) {

  } ngOnInit(): void {
    this.AllBagage();
  }
  AllBagage(){
    this.allService.getListeBagage().subscribe({
      next:data=>{
        this.bagages=data;
      },error:err=>{
        console.log(err);
      }
    });
  }
  handleVoirplus(data: Bagage) {
    this.router.navigateByUrl("admin/detailBagage/"+data.id);
    }
}
