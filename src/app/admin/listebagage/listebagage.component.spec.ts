import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListebagageComponent } from './listebagage.component';

describe('ListebagageComponent', () => {
  let component: ListebagageComponent;
  let fixture: ComponentFixture<ListebagageComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ListebagageComponent]
    });
    fixture = TestBed.createComponent(ListebagageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
