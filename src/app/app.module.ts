import { NgModule } from '@angular/core';

import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RetardComponent } from './retard/retard.component';
import { AnnulationComponent } from './annulation/annulation.component';
import { SurbookingComponent } from './surbooking/surbooking.component';
import { BagagesComponent } from './bagages/bagages.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzStepsModule } from 'ng-zorro-antd/steps';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzSpaceModule } from 'ng-zorro-antd/space';
import { NzRadioModule } from 'ng-zorro-antd/radio';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzUploadModule } from 'ng-zorro-antd/upload';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NzSelectModule } from 'ng-zorro-antd/select';
import {fr_FR, NZ_I18N} from 'ng-zorro-antd/i18n';
import { NzInputNumberModule } from 'ng-zorro-antd/input-number';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzMessageModule } from 'ng-zorro-antd/message';
import { NavbarComponent } from './navbar/navbar.component';
import { HomeComponent } from './home/home.component';
import { CarouselComponent } from './carousel/carousel.component';
import { NzCarouselModule } from 'ng-zorro-antd/carousel';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { NzTabsModule } from 'ng-zorro-antd/tabs';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { ContactComponent } from './contact/contact.component';
import { SuiviComponent } from './suivi/suivi.component';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { FaqComponent } from './faq/faq.component';
import { ListeannulationComponent } from './admin/listeannulation/listeannulation.component';
import { NzTableModule } from 'ng-zorro-antd/table';
import { DetailAnnulationComponent } from './admin/detail-annulation/detail-annulation.component';
import { TemplateComponent } from './admin/template/template.component';
import { ListebagageComponent } from './admin/listebagage/listebagage.component';

import { DetailBagageComponent } from './admin/detail-bagage/detail-bagage.component';
import { ListesurbookingComponent } from './admin/listesurbooking/listesurbooking.component';
import { DetailSurbookingComponent } from './admin/detail-surbooking/detail-surbooking.component';
import { ListeretardComponent } from './admin/listeretard/listeretard.component';
import { DetailRetardComponent } from './admin/detail-retard/detail-retard.component';
import { LoginComponent } from './login/login.component';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzPageHeaderModule } from 'ng-zorro-antd/page-header';
import { NzBreadCrumbModule } from 'ng-zorro-antd/breadcrumb';
@NgModule({
  declarations: [
    AppComponent,
    RetardComponent,
    AnnulationComponent,
    SurbookingComponent,
    BagagesComponent,
    NavbarComponent,
    HomeComponent,
    CarouselComponent,
    ContactComponent,
    SuiviComponent,
    FaqComponent,
    ListeannulationComponent,
    DetailAnnulationComponent,
    TemplateComponent,
    ListebagageComponent,

    DetailBagageComponent,
    ListesurbookingComponent,
    DetailSurbookingComponent,
    ListeretardComponent,
    DetailRetardComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule,
    NzButtonModule,
    NzStepsModule,
    NzFormModule,
    NzSpaceModule,
    NzRadioModule,
    NzInputModule ,
    NzUploadModule,
    NzSelectModule,
    BrowserAnimationsModule,
    NzIconModule,
    NzInputNumberModule,
    NzMessageModule,
    CarouselModule,
    NzTabsModule,
    SweetAlert2Module,
    NzSpinModule,
    NzModalModule,
    NzTableModule,
    NzMenuModule,
    NzPageHeaderModule,
    NzBreadCrumbModule

  ],
  providers: [ {provide: NZ_I18N, useValue: fr_FR}],
  bootstrap: [AppComponent]
})
export class AppModule { }
