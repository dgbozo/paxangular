import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AllServiceService } from '../service/all-service.service';
import { Router } from '@angular/router';
import { NzIconService } from 'ng-zorro-antd/icon';
import Swal from 'sweetalert2/dist/sweetalert2.js';
@Component({
  selector: 'app-suivi',
  templateUrl: './suivi.component.html',
  styleUrls: ['./suivi.component.css']
})
export class SuiviComponent {
  validateForm: any;
  data: any;
  constructor(private fb: FormBuilder, public allService:AllServiceService  ,private route: Router, private nzIconService: NzIconService) {




  }
  ngOnInit(): void {




    this.validateForm = this.fb.group({
      formLayout: ['vertical'],


      numeroDemande: ['',[Validators.required]],




  });
}
simpleAlert(messag:any){

  Swal.fire({
    title: 'Indemnisation',
    text: messag,
    denyButtonText: `OK`,
})

}
simpleAlertError(messag:any){

  Swal.fire({
    title: 'statut',
    text: messag,
    denyButtonText: `OK`,
})

}
submitForm(): void {
  console.log('submit', this.validateForm.value);

const formData = new FormData();
formData.append("numeroDemande",this.validateForm.value['numeroDemande']);

this.allService.getStatutDemande(formData).subscribe(res => {
  this.data = res;

  if (this.data.statut) {
    this.simpleAlert(this.data.statut);
  }

  console.log(this.data.success);
});

}
}
