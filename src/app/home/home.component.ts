import { Component } from '@angular/core';
import { AllServiceService } from '../service/all-service.service';
import { FormBuilder, NgForm, FormGroup, UntypedFormBuilder, UntypedFormGroup, Validators, FormControl } from '@angular/forms';
import { Airports } from '../model/airports';
import { Router } from '@angular/router';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { NzIconService } from 'ng-zorro-antd/icon';
import { StepForwardOutline } from '@ant-design/icons-angular/icons';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  airports!: Array<Airports> ;
  selectedValue = null;
  selectedValue1 = null;
  data: any;
  validateForm!: UntypedFormGroup;
  validateFormAnnulation!: UntypedFormGroup;
  validateFormBagage!: UntypedFormGroup;
  validateFormSurbooking!: UntypedFormGroup;
  errorMessage! : string;
  constructor(private fb: FormBuilder, public allService:AllServiceService  ,private route: Router, private nzIconService: NzIconService) {
    this.nzIconService.addIcon(StepForwardOutline);
  }
  ngOnInit(): void {

    this.AllAirports();


    this.validateForm = this.fb.group({
      formLayout: ['vertical'],


      depart: ['',[Validators.required]],
     arrive: ['', [Validators.required]],
     dureeretard: ['', [Validators.required]],
     typeretard: ['', [Validators.required]],



  });
  this.validateFormAnnulation = this.fb.group({
    formLayout: ['vertical'],


    depart: ['',[Validators.required]],
   arrive: ['', [Validators.required]],




});
this.validateFormSurbooking = this.fb.group({
  formLayout: ['vertical'],


  depart: ['',[Validators.required]],
 arrive: ['', [Validators.required]],

 classeplace: ['', [Validators.required]],


});

this.validateFormBagage = this.fb.group({
  formLayout: ['vertical'],


  naturebagage: ['',[Validators.required]],
 poids: ['', [Validators.required]],




});

  }
  simpleAlert(messag:any){

    Swal.fire({
      title: 'Indemnisation',
      text: messag,
      denyButtonText: `OK`,
  })

  }
  simpleAlert1(messag:any,lien:any){

    Swal.fire({
      title: 'Indemnisation',
      text: messag,
      showDenyButton: true,
      confirmButtonText: lien != '' ? 'Déposer' : 'OK',
      denyButtonText: `Quitter`,
  }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
          /*Swal.fire('{{ Session::get('backlink') }}', '', 'success')*/
          this.route.navigate([lien]);

      } else if (result.isDenied && lien != '') {
          Swal.fire({

              showCancelButton: true,

              confirmButtonText: 'Déposer',
              title: 'Indemnisation',
              text: 'Plus de 35 millions d\'euros ont déjà été payés suite à une procédure d\'idemnisation. Nos experts en droit aérien s\'occuperont de tout pour que vous entriez dans vos droits sans vous déplacer. Nous vous encourageons à poursuivre le processus car vous n\'y perdez absolument rien.',
          }).then((result) => {
              if (result.isConfirmed) {
                  // Swal.fire('{{ Session::get('backlink') }}', '', 'success')

                  this.route.navigate([lien]);
              }
          })
      }
  });

  }

    AllAirports(){
      this.allService.getAllairport().subscribe({
        next:data=>{
          this.airports=data;
        },error:err=>{
          console.log(err);
        }
      });
    }
    submitForm(): void {
      console.log('submit', this.validateForm.value);

    const formData = new FormData();
    formData.append("depart",this.validateForm.value['depart']);
    formData.append("arrive", this.validateForm.value['arrive']);
    formData.append("dureeretard", this.validateForm.value['dureeretard']);
    formData.append("typeretard", this.validateForm.value['typeretard']);
    this.allService.calculRetard(formData).subscribe(res => {
      this.data = res;
      if (!this.data.backlink) {
        this.simpleAlert(this.data.success);
      }else{
        this.simpleAlert1(this.data.success,this.data.backlink)
      }


      console.log(this.data.success);
    });

    }
    submitFormAnnulation(): void {
      console.log('submit', this.validateFormAnnulation.value);

    const formData = new FormData();
    formData.append("depart",this.validateFormAnnulation.value['depart']);
    formData.append("arrive", this.validateFormAnnulation.value['arrive']);

    this.allService.calculAnnulation(formData).subscribe(res => {
      this.data = res;
      if (!this.data.backlink) {
        this.simpleAlert(this.data.success);
      }else{
        this.simpleAlert1(this.data.success,this.data.backlink)
      }


      console.log(this.data.success);
    });

    }
    submitFormSurbooking(): void {
      console.log('submit', this.validateFormSurbooking.value);

    const formData = new FormData();
    formData.append("depart",this.validateFormSurbooking.value['depart']);
    formData.append("arrive", this.validateFormSurbooking.value['arrive']);
    formData.append("classeplace", this.validateFormSurbooking.value['classeplace']);

    this.allService.calculSurbooking(formData).subscribe(res => {
      this.data = res;
      if (!this.data.backlink) {
        this.simpleAlert(this.data.success);
      }else{
        this.simpleAlert1(this.data.success,this.data.backlink)
      }


      console.log(this.data.success);
    });

    }
    submitFormBagage(): void {
      console.log('submit', this.validateFormBagage.value);

    const formData = new FormData();
    formData.append("naturebagage",this.validateFormBagage.value['naturebagage']);
    formData.append("poids", this.validateFormBagage.value['poids']);

    this.allService.calculBagage(formData).subscribe(res => {
      this.data = res;
      if (!this.data.backlink) {
        this.simpleAlert(this.data.success);
      }else{
        this.simpleAlert1(this.data.success,this.data.backlink)
      }


      console.log(this.data.success);
    });

    }
}
