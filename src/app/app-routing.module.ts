import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AnnulationComponent } from './annulation/annulation.component';
import { BagagesComponent } from './bagages/bagages.component';
import { RetardComponent } from './retard/retard.component';
import { Surbooking } from './model/surbooking';
import { SurbookingComponent } from './surbooking/surbooking.component';
import { HomeComponent } from './home/home.component';
import { ContactComponent } from './contact/contact.component';
import { SuiviComponent } from './suivi/suivi.component';
import { FaqComponent } from './faq/faq.component';
import { ListeannulationComponent } from './admin/listeannulation/listeannulation.component';
import { DetailAnnulationComponent } from './admin/detail-annulation/detail-annulation.component';
import { TemplateComponent } from './admin/template/template.component';

import { LoginComponent } from './login/login.component';
import { authGuardGuard} from './auth-guard.guard';
import { ListesurbookingComponent } from './admin/listesurbooking/listesurbooking.component';
import { ListeretardComponent } from './admin/listeretard/listeretard.component';
import { ListebagageComponent } from './admin/listebagage/listebagage.component';
import { DetailBagageComponent } from './admin/detail-bagage/detail-bagage.component';
import { DetailSurbookingComponent } from './admin/detail-surbooking/detail-surbooking.component';
import { DetailRetardComponent } from './admin/detail-retard/detail-retard.component';

const routes: Routes = [{path:"annulation",component:AnnulationComponent},
{path:"contact",component:ContactComponent},
{path:"login",component:LoginComponent},
{path:"suivi",component:SuiviComponent},
{path:"faq",component:FaqComponent},
{path:"bagage",component:BagagesComponent}
,{path:"",component:HomeComponent},
{path:"surbooking",component:SurbookingComponent}
,
{path:"retard",component:RetardComponent},



{path:"admin",component:TemplateComponent,canActivate:[authGuardGuard],
 children:[
    {path:"detailAnnulation/:id",component:DetailAnnulationComponent},
    {path:"listeAnnulation",component:ListeannulationComponent},

    {path:"listeSurbooking",component:ListesurbookingComponent},
    {path:"detailSurbooking/:id",component:DetailSurbookingComponent},

    {path:"listeRetard",component:ListeretardComponent},
    {path:"detailRetard/:id",component:DetailRetardComponent},

     
    {path:"detailBagage/:id",component:DetailBagageComponent},
    {path:"listeBagage",component:ListebagageComponent},
 

  ]},
];



@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
